"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var ts = require("typescript");
var Lint = require("tslint");
var Rule = /** @class */ (function (_super) {
    __extends(Rule, _super);
    function Rule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * Initialize rule
     *
     * @param sourceFile
     *
     * @returns {RuleFailure[]}
     */
    Rule.prototype.apply = function (sourceFile) {
        return this.applyWithWalker(new ActualDocWalker(sourceFile, this.getOptions()));
    };
    /**
     * Factory of failure message
     *
     * @param memberType
     * @param memberName
     *
     * @returns {string}
     */
    Rule.FAILURE_STRING_FACTORY = function (memberType, memberName) {
        memberName = memberName.length ? "'" + memberName + "'" : '';
        return "JsDoc params mismatch in " + memberType + " " + memberName;
    };
    return Rule;
}(Lint.Rules.AbstractRule));
exports.Rule = Rule;
var ActualDocWalker = /** @class */ (function (_super) {
    __extends(ActualDocWalker, _super);
    function ActualDocWalker() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ActualDocWalker.prototype.visitConstructorDeclaration = function (node) {
        this.validateJsDocComment(node);
        _super.prototype.visitConstructorDeclaration.call(this, node);
    };
    ActualDocWalker.prototype.visitMethodSignature = function (node) {
        this.validateJsDocComment(node);
        _super.prototype.visitMethodSignature.call(this, node);
    };
    ActualDocWalker.prototype.visitFunctionDeclaration = function (node) {
        this.validateJsDocComment(node);
        _super.prototype.visitFunctionDeclaration.call(this, node);
    };
    ActualDocWalker.prototype.visitMethodDeclaration = function (node) {
        this.validateJsDocComment(node);
        _super.prototype.visitMethodDeclaration.call(this, node);
    };
    /**
     * Validate doc node
     *
     * @param node
     */
    ActualDocWalker.prototype.validateJsDocComment = function (node) {
        var _this = this;
        var nodeType = this.getNodeTypeText(node);
        var nodeName = '';
        var docParams = [];
        var signatureParams = [];
        node.getChildren().forEach(function (childNode) {
            if (_this.isNodeKindOf(childNode, ts.SyntaxKind.Identifier)) {
                nodeName = childNode.getText();
            }
            if (_this.isNodeKindOf(childNode, ts.SyntaxKind.JSDocComment)) {
                docParams = _this.retrieveDocParams(childNode);
            }
            if (_this.isNodeKindOf(childNode, ts.SyntaxKind.SyntaxList)) {
                signatureParams = _this.retrieveSignatureParams(childNode);
            }
        });
        /**
         * If params defined in docs compare them with signature
         */
        if (docParams.length && !this.isEqualDocParamsToSignature(docParams, signatureParams)) {
            var failureString = Rule.FAILURE_STRING_FACTORY(nodeType, nodeName);
            this.addFailureAtNode(node, failureString);
        }
    };
    /**
     * Parse signature params
     *
     * @param signatureNode
     *
     * @returns {string[]}
     */
    ActualDocWalker.prototype.retrieveSignatureParams = function (signatureNode) {
        var _this = this;
        var params = [];
        signatureNode.getChildren().forEach(function (syntaxNode) {
            if (_this.isNodeKindOf(syntaxNode, ts.SyntaxKind.Parameter)) {
                syntaxNode.getChildren().forEach(function (parameterNode, index) {
                    if (_this.isNodeKindOf(parameterNode, ts.SyntaxKind.Identifier)
                        && !_this.previousNodeKindOf(index, ts.SyntaxKind.EqualsToken, syntaxNode)) {
                        params.push(parameterNode.getText());
                    }
                });
            }
        });
        return params;
    };
    /**
     * Parse doc params
     *
     * @param docNode
     *
     * @returns {string[]}
     */
    ActualDocWalker.prototype.retrieveDocParams = function (docNode) {
        var _this = this;
        var params = [];
        docNode.getChildren().forEach(function (commentNode) {
            if (_this.isNodeKindOf(commentNode, ts.SyntaxKind.SyntaxList)) {
                commentNode.getChildren().forEach(function (syntaxElement) {
                    if (_this.isNodeKindOf(syntaxElement, ts.SyntaxKind.JSDocParameterTag)) {
                        syntaxElement.getChildren().forEach(function (parameterNode, parameterIndex) {
                            if (_this.isNodeKindOf(parameterNode, ts.SyntaxKind.Identifier)
                                && !_this.previousNodeKindOf(parameterIndex, ts.SyntaxKind.AtToken, syntaxElement)) {
                                params.push(parameterNode.getText());
                            }
                        });
                    }
                });
            }
        });
        return params;
    };
    /**
     * Check previous node for kind of
     *
     * @param currentNodeIndex
     * @param kind
     * @param parentNode
     *
     * @returns {boolean}
     */
    ActualDocWalker.prototype.previousNodeKindOf = function (currentNodeIndex, kind, parentNode) {
        if (currentNodeIndex > 0) {
            var previous = parentNode.getChildAt(currentNodeIndex - 1);
            return this.isNodeKindOf(previous, kind);
        }
        return false;
    };
    /**
     * Check for kind of
     *
     * @param node
     * @param kind
     *
     * @returns {boolean}
     */
    ActualDocWalker.prototype.isNodeKindOf = function (node, kind) {
        return node.kind === kind;
    };
    /**
     * Return user friendly kind of node
     *
     * @param node
     *
     * @returns {string}
     */
    ActualDocWalker.prototype.getNodeTypeText = function (node) {
        var memberTypeText;
        switch (node.kind) {
            case ts.SyntaxKind.MethodSignature:
                memberTypeText = 'interface declaration';
                break;
            case ts.SyntaxKind.FunctionDeclaration:
                memberTypeText = 'function declaration';
                break;
            case ts.SyntaxKind.MethodDeclaration:
                memberTypeText = 'class method';
                break;
            case ts.SyntaxKind.Constructor:
                memberTypeText = 'class constructor';
                break;
            default:
                memberTypeText = '';
                break;
        }
        return memberTypeText;
    };
    /**
     * Compare doc params with node signature
     *
     * @param docParams
     * @param constructionSignature
     *
     * @returns {boolean}
     */
    ActualDocWalker.prototype.isEqualDocParamsToSignature = function (docParams, constructionSignature) {
        return docParams.length === constructionSignature.length && docParams.every(function (element, index) {
            return element === constructionSignature[index];
        });
    };
    return ActualDocWalker;
}(Lint.RuleWalker));
//# sourceMappingURL=actualJsDocRule.js.map