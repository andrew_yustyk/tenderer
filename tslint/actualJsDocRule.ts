import * as ts from 'typescript';
import * as Lint from 'tslint';

export class Rule extends Lint.Rules.AbstractRule {
  /**
   * Factory of failure message
   *
   * @param memberType
   * @param memberName
   *
   * @returns {string}
   */
  public static FAILURE_STRING_FACTORY = function (memberType: string, memberName: string) {
    memberName = memberName.length ? `'${memberName}'` : '';
    return `JsDoc params mismatch in ${memberType} ${memberName}`;
  };

  /**
   * Initialize rule
   *
   * @param sourceFile
   *
   * @returns {RuleFailure[]}
   */
  public apply(sourceFile: ts.SourceFile): Lint.RuleFailure[] {
    return this.applyWithWalker(new ActualDocWalker(sourceFile, this.getOptions()));
  }
}

class ActualDocWalker extends Lint.RuleWalker {

  public visitConstructorDeclaration(node: ts.ConstructorDeclaration) {
    this.validateJsDocComment(node);
    super.visitConstructorDeclaration(node);
  }

  public visitMethodSignature(node: ts.SignatureDeclaration) {
    this.validateJsDocComment(node);
    super.visitMethodSignature(node);
  }

  public visitFunctionDeclaration(node: ts.FunctionDeclaration) {
    this.validateJsDocComment(node);
    super.visitFunctionDeclaration(node);
  }

  public visitMethodDeclaration(node: ts.MethodDeclaration) {
    this.validateJsDocComment(node);
    super.visitMethodDeclaration(node);
  }

  /**
   * Validate doc node
   *
   * @param node
   */
  private validateJsDocComment(node: ts.Node): void {
    const nodeType = this.getNodeTypeText(node);

    let nodeName = '';
    let docParams = [];
    let signatureParams = [];

    node.getChildren().forEach((childNode: ts.Node) => {

      if (this.isNodeKindOf(childNode, ts.SyntaxKind.Identifier)) {
        nodeName = childNode.getText();
      }

      if (this.isNodeKindOf(childNode, ts.SyntaxKind.JSDocComment)) {
        docParams = this.retrieveDocParams(childNode);
      }

      if (this.isNodeKindOf(childNode, ts.SyntaxKind.SyntaxList)) {
        signatureParams = this.retrieveSignatureParams(childNode);
      }
    });

    /**
     * If params defined in docs compare them with signature
     */
    if (docParams.length && !this.isEqualDocParamsToSignature(docParams, signatureParams)) {
      const failureString = Rule.FAILURE_STRING_FACTORY(nodeType, nodeName);
      this.addFailureAtNode(node, failureString);
    }
  }

  /**
   * Parse signature params
   *
   * @param signatureNode
   *
   * @returns {string[]}
   */
  private retrieveSignatureParams(signatureNode: ts.Node): string[] {
    const params: string[] = [];
    signatureNode.getChildren().forEach((syntaxNode: ts.Node) => {
      if (this.isNodeKindOf(syntaxNode, ts.SyntaxKind.Parameter)) {
        syntaxNode.getChildren().forEach((parameterNode: ts.Node, index) => {
          if (this.isNodeKindOf(parameterNode, ts.SyntaxKind.Identifier)
            && !this.previousNodeKindOf(index, ts.SyntaxKind.EqualsToken, syntaxNode)) {
            params.push(parameterNode.getText());
          }
        });
      }
    });
    return params;
  }

  /**
   * Parse doc params
   *
   * @param docNode
   *
   * @returns {string[]}
   */
  private retrieveDocParams(docNode: ts.Node): string[] {
    const params: string[] = [];
    docNode.getChildren().forEach((commentNode: ts.Node) => {
      if (this.isNodeKindOf(commentNode, ts.SyntaxKind.SyntaxList)) {
        commentNode.getChildren().forEach((syntaxElement: ts.Node) => {
          if (this.isNodeKindOf(syntaxElement, ts.SyntaxKind.JSDocParameterTag)) {
            syntaxElement.getChildren().forEach((parameterNode: ts.Node, parameterIndex) => {
              if (this.isNodeKindOf(parameterNode, ts.SyntaxKind.Identifier)
                && !this.previousNodeKindOf(parameterIndex, ts.SyntaxKind.AtToken, syntaxElement)) {
                params.push(parameterNode.getText());
              }
            });
          }
        });
      }
    });
    return params;
  }

  /**
   * Check previous node for kind of
   *
   * @param currentNodeIndex
   * @param kind
   * @param parentNode
   *
   * @returns {boolean}
   */
  private previousNodeKindOf(currentNodeIndex, kind: number, parentNode: ts.Node): boolean {
    if (currentNodeIndex > 0) {
      const previous: ts.Node = parentNode.getChildAt(currentNodeIndex - 1);
      return this.isNodeKindOf(previous, kind);
    }
    return false;
  }

  /**
   * Check for kind of
   *
   * @param node
   * @param kind
   *
   * @returns {boolean}
   */
  private isNodeKindOf(node: ts.Node, kind: number) {
    return node.kind === kind;
  }

  /**
   * Return user friendly kind of node
   *
   * @param node
   *
   * @returns {string}
   */
  private getNodeTypeText(node: ts.Node): string {
    let memberTypeText: string;

    switch (node.kind) {
      case ts.SyntaxKind.MethodSignature:
        memberTypeText = 'interface declaration';
        break;
      case ts.SyntaxKind.FunctionDeclaration:
        memberTypeText = 'function declaration';
        break;
      case ts.SyntaxKind.MethodDeclaration:
        memberTypeText = 'class method';
        break;
      case ts.SyntaxKind.Constructor:
        memberTypeText = 'class constructor';
        break;
      default:
        memberTypeText = '';
        break;
    }

    return memberTypeText;
  }

  /**
   * Compare doc params with node signature
   *
   * @param docParams
   * @param constructionSignature
   *
   * @returns {boolean}
   */
  private isEqualDocParamsToSignature(docParams: string[], constructionSignature: string []) {
    return docParams.length === constructionSignature.length && docParams.every((element, index) => {
        return element === constructionSignature[index];
      });
  }
}
